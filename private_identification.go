package classes

type PrivateIdentification struct {
	BirthDate      string `json:"birth_date"`
	BirthCountry   string `json:"birth_country"`
	Identification string `json:"identification"`
	Address        string `json:"address"`
	City           string `json:"city"`
	Country        string `json:"country"`
}
