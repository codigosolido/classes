package classes

type Links struct {
	Self  string `json:"self"`
	First string `json:"first"`
	Last  string `json:"last"`
}
