# Form3 Take Home Excersice Classes Library

This repository is part of the project [Form3](https://bitbucket.org/codigosolido/workspace/projects/FOR) as technical exercise made by Juan Diego Munoz for Form3, as part of the enrolment proccess at [Form3](https://github.com/form3tech-oss/interview-accountapi/blob/master/README.md).

This library implements the classes to operate against the [From3 API](http://api-docs.form3.tech/api.html#organisation-accounts).

## Structure

In the root folder we have the files defining the differents structures to interact with the API. The data hierarchy is as follow

- Message
  - Data [Resource]
    - Type           [string]
    - Id             [string]
    - OrganisationId [string]
    - Version        [int]
    - Created        [string]
    - Modified       [string]
    - Attributes     [Account]
        - Country                 [string]
        - BaseCurrency            [string]
        - BankId                  [string]
        - BankIdCode              [string]
        - AccountNumber           [string]
        - Bic                     [string]
        - Iban                    [string]
        - CustomerId              [string]
        - Name                    [4][string]
        - AlternativeNames        [3][string]
        - AccountClassification   [AccountClassification]
        - JointAccount            [bool]
        - AccountMatchingOptOut   [bool]
        - SecondaryIdentification [string]
        - Switched                [bool]
  - Links [Links]
    - Self  [string]
    - First [string]
    - Last  [string]

