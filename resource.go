package classes

type Resource struct {
	Type           string  `json:"type"`
	Id             string  `json:"id"`
	OrganisationId string  `json:"organisation_id"`
	Version        int     `json:"version"`
	Created        string  `json:"created_on"`
	Modified       string  `json:"modified_on"`
	Attributes     Account `json:"attributes"` // If attributes can be other type, this can be an interface{}
}
